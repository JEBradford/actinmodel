/*
 *  pythoncaller.cpp
 *
 *
 *  Function that calls python and runs the python script to plot the frames
 *  of the simulation automatically
 *  Must have python3-dev installed ("sudo apt-get install python3-dev")
 *
 *  James Bradford
 *  University of Sheffield
 *  May 2017
 */

#include "configHeader.h"
#include "pythoncaller.h"

void calltoplotActinframes(std::string outdir, double TPF, std::string FT, std::string FL,
                           bool test)
{
        // Call python script to plot frames
        // Write as a function, inputs: outdir

        int argc = 10;
        const wchar_t *const_argv[argc];

        const_argv[0] = L"plottingScripts/plotActinframes.py";
        const_argv[1] = L"-I";

        std::wstring w_outdir(outdir.length(), L' '); // Make room for characters

        // Copy string to wstring.
        std::copy(outdir.begin(), outdir.end(), w_outdir.begin());

        std::wstring results_dir = w_outdir + std::wstring(L"/results.dat");
        const_argv[2] = results_dir.c_str();
        const_argv[3] = L"-O";


        std::wstring frames_dir;
        if (test)
        {
                w_outdir.erase(0,13);
                frames_dir = std::wstring(L"../testPlotting/testFrames") + w_outdir;

        }
        else
        {
                w_outdir.erase(0,9);
                frames_dir = std::wstring(L"../plotting/frames") + w_outdir;
        }
        std::wcout << frames_dir << std::endl;
        const_argv[4] = frames_dir.c_str();

        const_argv[5] = L"-TPF";
        std::wstringstream s;
        s << TPF;
        std::wstring s2 = s.str();
        const_argv[6] = s2.c_str();

        const_argv[7] = L"-FT";
        std::wstring str2(FT.length(), L' '); // Make room for characters
        // Copy string to wstring.
        std::copy(FT.begin(), FT.end(), str2.begin());
        const_argv[8] = str2.c_str();


        // Work around for negative xmins
        std::wstring Fl = L"-FL=";
        std::wstring str3(FL.length(), L' '); // Make room for characters
        // Copy string to wstring.
        std::copy(FL.begin(), FL.end(), str3.begin());
        std::wstring FLarg = Fl + str3;
        const_argv[9] = FLarg.c_str();



        wchar_t *argv[argc];
        for (int i = 0; i < argc; ++i)
                argv[i] = (wchar_t*)const_argv[i];


        Py_Initialize();
        PySys_SetArgv(argc, argv);
        FILE *fp = fopen("plottingScripts/plotActinframes.py", "r");
        PyRun_SimpleFile(fp, "plottingScripts/plotActinframes.py");
        Py_Finalize();
}
