# Makefile for desktop

CC := g++
SRCDIR := src
BUILDDIR := build
BUILDDIR_DEBUG := build_debug
TARGET := bin/model
TARGETDB := bin/model_debug
INCDIR := include
EIGEN := /home/$(USER)/eigen-3.3.9
GEO := GeometricTools/GTEngine/Include
PYT := /home/$(USER)/anaconda3/include/python3.8/
CONPYT := /home/$(USER)/anaconda3/lib
RM := rm -f

CFLAGS := -O3 -std=c++14 -DEIGEN_NO_DEBUG -Wall -fopenmp
CFLAGS_DEBUG := -Wall -std=c++14 -fopenmp -DBOOST_TEST_DYN_LINK -g

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
OBJECTS_DEBUG := $(patsubst $(SRCDIR)/%,$(BUILDDIR_DEBUG)/%,$(SOURCES:.$(SRCEXT)=.o))

INC := -I $(INCDIR) -I $(EIGEN) -I $(GEO) -I $(PYT)
LIB := -L $(CONPYT) -lpython3.8 -lboost_program_options -lutil
 
$(TARGET): $(OBJECTS)
	  @echo " Linking..."
	    @echo " $(CC) -fopenmp $^ -o $(TARGET) $(LIB)"; $(CC) -fopenmp $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	  @mkdir -p $(BUILDDIR)
	    @echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(BUILDDIR_DEBUG)/%.o: $(SRCDIR)/%.$(SRCEXT)
	  @mkdir -p $(BUILDDIR_DEBUG)
	    @echo " $(CC) $(CFLAGS_DEBUG) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS_DEBUG) $(INC) -c -o $@ $<


debug: $(OBJECTS_DEBUG)
	$(CC) $(CFLAGS_DEBUG) $(OBJECTS_DEBUG) $(INC) $(LIB) -o $(TARGETDB)


.PHONY: clean
clean:
	  @echo " Cleaning...";
	    @echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

clean_debug:
	  @echo " Cleaning...";
	    @echo " $(RM) -r $(BUILDDIR_DEBUG) $(TARGET)"; $(RM) -r $(BUILDDIR_DEBUG) $(TARGETDB)
